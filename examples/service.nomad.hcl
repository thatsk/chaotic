variable "datacenters" {
  type = list(string)
  default = ["dc1"]
}

job "ngine-io.chaotic" {
  datacenters = var.datacenters
  namespace = "default"

  type = "service"

  group "chaos" {
    count = 1

    task "chaotic" {
      template {
        change_mode = "noop"
        destination = "local/config.yaml"
        data = <<EOH
---
kind: nomad
dry_run: false
excludes:
  weekdays:
    - Sun
    - Sat
  times_of_day:
    - 22:00-08:00
    - 11:00-14:00
  days_of_year:
    - Jan01
    - Apr01
    - May01
    - Aug01
    - Dec24
configs:
  namespace_denylist:
    - default
  signals:
    - SIGKILL
EOH
    }

    env = {
      TZ = "Europe/Zurich"
      NOMAD_ADDR = "http://172.17.0.1:4646"
      CHAOTIC_CONFIG = "/app/config.yaml"
    }

    driver = "docker"
      config {
        image = "registry.gitlab.com/ngine/docker-images/chaotic:latest"
        args = [
            "--periodic"
        ]
        force_pull = true
        volumes = [
          "local/config.yaml:/app/config.yaml",
        ]
      }
    }
  }
}
