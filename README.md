# Chaotic

Docker images of https://github.com/ngine-io/chaotic.

## Examples

For nomad job definitions, see examples.

## Docker

One shot:

```
docker run -ti --rm -v $PWD/examples/config_nomad.yaml:/app/config.yaml -e TZ=Europe/Zurich -e NOMAD_ADDR=$NOMAD_ADDR --name chaotic registry.gitlab.com/ngine/docker-images/chaotic
```

As service:

```
docker run -ti --rm -v $PWD/examples/config_nomad.yaml:/app/config.yaml -e TZ=Europe/Zurich -e NOMAD_ADDR=$NOMAD_ADDR --name chaotic registry.gitlab.com/ngine/docker-images/chaotic --periodic
